#!/usr/bin/env python3

import cv2
import numpy as np
import depthai as dai
from time import sleep
import datetime
import argparse
import subprocess as sp

if __name__ == "__main__":
    FPS = 30
    pipeline = dai.Pipeline()

    lCam = pipeline.create(dai.node.MonoCamera)
    lCam.setCamera("left")
    lCam.setBoardSocket(dai.CameraBoardSocket.LEFT)
    
    lvideoEnc = pipeline.create(dai.node.VideoEncoder)
    lvideoEnc.setDefaultProfilePreset(FPS, dai.VideoEncoderProperties.Profile.H264_MAIN)
    lvideoEnc.setKeyframeFrequency(FPS * 4)
    lvideoEnc.setQuality(100)

    lCam.out.link(lvideoEnc.input)    

    xout_videoL = pipeline.create(dai.node.XLinkOut)
    xout_videoL.setStreamName("encodedL")

    lvideoEnc.bitstream.link(xout_videoL.input)



    rCam = pipeline.create(dai.node.MonoCamera)
    rCam.setCamera("right")
    rCam.setBoardSocket(dai.CameraBoardSocket.RIGHT)


    rvideoEnc = pipeline.create(dai.node.VideoEncoder)
    rvideoEnc.setDefaultProfilePreset(FPS, dai.VideoEncoderProperties.Profile.H264_MAIN)
    rvideoEnc.setKeyframeFrequency(FPS * 4)
    rvideoEnc.setQuality(100)

    rCam.out.link(rvideoEnc.input)


    xout_videoR = pipeline.create(dai.node.XLinkOut)
    xout_videoR.setStreamName("encodedR")

    rvideoEnc.bitstream.link(xout_videoR.input)


    cam = pipeline.create(dai.node.ColorCamera)
    cam.setPreviewSize(540,540)
    cam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    cam.setInterleaved(False)
    cam.setBoardSocket(dai.CameraBoardSocket.RGB)
   

    videoEnc = pipeline.create(dai.node.VideoEncoder)
    videoEnc.setDefaultProfilePreset(FPS, dai.VideoEncoderProperties.Profile.H264_MAIN)
    videoEnc.setKeyframeFrequency(FPS * 4)
    videoEnc.setQuality(100)

    cam.video.link(videoEnc.input)

    xout_video = pipeline.create(dai.node.XLinkOut)
    xout_video.setStreamName("encoded")

    videoEnc.bitstream.link(xout_video.input)





    with dai.Device(pipeline=pipeline) as device:
        q = device.getOutputQueue("encoded")
        lQ = device.getOutputQueue("encodedL")
        rQ = device.getOutputQueue("encodedR")
        command = [
       "ffmpeg",
           "-probesize", "32",
           "-i", "-",
           "-f", "rtsp",
           "-rtsp_transport", "tcp",
           "-framerate", "30",
           "-vcodec", "copy",
           "-v", "error",
           "-fflags", "nobuffer",
           "rtsp://192.168.0.195:8554/preview"
        ]

        commandL = [
       "ffmpeg",
           "-probesize", "32",
           "-i", "-",
           "-f", "rtsp",
           "-rtsp_transport", "tcp",
           "-framerate", "30",
           "-vcodec", "copy",
           "-v", "error",
           "-fflags", "nobuffer",
           "rtsp://192.168.0.195:8554/leftCam"
        ]

        commandR = [
       "ffmpeg",
           "-probesize", "32",
           "-i", "-",
           "-f", "rtsp",
           "-rtsp_transport", "tcp",
           "-framerate", "30",
           "-vcodec", "copy",
           "-v", "error",
           "-fflags", "nobuffer",
           "rtsp://192.168.0.195:8554/rightCam"
        ]
        # command= [
        #     "ffmpeg",
        #     "-f", "v4l2",
        #     "-i", "-",
        #     "-profile:v", "high",
        #     "-pix_fmt", "yuvj420p",
        #     "-level:v", "4.1",
        #     "-preset", "ultrafast",
        # "-tune", "zerolatency",
        #     "-vcodec", "libx264",
        #     "-r", "10",
        #     "-b:v", "512k",
        #     "-s", "640x360",
        #     "-strict", "-2",
        #     "-f", "mpegts",
        #     "-flush_packets", "0",
        #     "udp://192.168.0.222:5000?pkt_size=1316"
        # ]

        
        try:
            proc1 = sp.Popen(commandL, stdin=sp.PIPE)
            proc2 = sp.Popen(command, stdin=sp.PIPE)
            proc3 = sp.Popen(commandR, stdin=sp.PIPE)
        except:
            exit("Error cannot run ffmpeg!\nTry running: sudo apt install ffmpeg")
        try:
            while True:
#                print("running camera stream now")
                data = lQ.get().getData()
                proc1.stdin.write(data)
                data = q.get().getData()
                proc2.stdin.write(data)
                data = rQ.get().getData()
                proc3.stdin.write(data)
        except Exception as e:
            pass
        proc1.stdin.close()
        proc2.stdin.close()
        proc3.stdin.close()

